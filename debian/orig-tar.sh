#!/bin/sh

VERSION=$2
TAR=../user-agent-utils_$VERSION.orig.tar.xz
DIR=user-agent-utils-$VERSION

mkdir $DIR
tar -xf $3 --strip-components=1 -C $DIR
rm $3

XZ_OPT=--best tar -cJvf $TAR --exclude 'javadoc/*' $DIR
rm -Rf $DIR
